# clothing_api

#### Assignment brief for junior developers

Spring WebFlux kirjastolla toteutettu sovellus, jonka tehtävänä on esittää tietoa kategorioittain ja helposti selattavassa muodossa seuraavan API:n datasta:

https://bad-api-assignment.reaktor.com

Sovellus noudattaa reaktiivisen ohjelmoinnin paradigmaa. Sovelluksen rajapinnat käsittelevät dataa Flux-striimeinä, joka mahdollistaa sivun päivittämisen sitä mukaa kun listan olioita lähetetään. Striimattavan datan vastaanottaminen tapahtuu oboe.js kirjastolla. Palvelin hakee ja tallentaa legacy API:n sisällön 5 minuutin välein muistiin haun nopeuttamiseksi.

Demo versio löytyy Herokusta:

https://clothing-rest-api.herokuapp.com


