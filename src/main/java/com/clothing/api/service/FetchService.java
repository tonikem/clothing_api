package com.clothing.api.service;

import java.io.IOException;
import reactor.core.publisher.Flux;
import java.net.http.HttpResponse.BodyHandlers;
import java.net.http.*;
import java.net.URI;
import org.json.*;


public class FetchService {

    private static String BASE_URL = "https://bad-api-assignment.reaktor.com/products/";

    public static Flux<?> fetchProductsByCategory(String category) {
        try {
            var client = HttpClient.newHttpClient();
            var url = URI.create(BASE_URL + category);
            var request = HttpRequest.newBuilder(url).build();
            var response = client.send(request, BodyHandlers.ofString());
            if (response.statusCode() != 200) throw new IOException();
            var jsonArray = new JSONArray(response.body());
            return Flux.fromIterable(jsonArray).map(Object::toString);
        }
        catch (InterruptedException | IOException e) {
            fetchProductsByCategory(category);
        }
        catch (StackOverflowError e) {
            System.err.println("Resursseja ei voitu hakea osoitteesta: "
                    + BASE_URL + category + ". Tarkista verkkoyhteys.");
        }
        return Flux.empty();
    }

}


