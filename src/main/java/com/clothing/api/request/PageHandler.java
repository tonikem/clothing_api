package com.clothing.api.request;

import org.springframework.web.reactive.function.server.*;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;


@Component
public class PageHandler {

    public Mono<ServerResponse> frontPageHandle(ServerRequest req) {
        return ServerResponse.ok().render("html/front_page.html");
    }

    public Mono<ServerResponse> accessoriesPageHandle(ServerRequest req) {
        return ServerResponse.ok().render("html/accessories.html");
    }

    public Mono<ServerResponse> jacketsPageHandle(ServerRequest req) {
        return ServerResponse.ok().render("html/jackets.html");
    }

    public Mono<ServerResponse> shirtsPageHandle(ServerRequest req) {
        return ServerResponse.ok().render("html/shirts.html");
    }

}


