package com.clothing.api.request;

import org.springframework.context.annotation.*;
import org.springframework.web.reactive.function.server.*;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;


@Configuration
public class PageRouter {

    @Bean("webPageRoute")
    public RouterFunction<ServerResponse> pageRoute(PageHandler handler) {
        return route(GET("/accessories"), handler::accessoriesPageHandle)
               .andRoute(GET("/jackets"), handler::jacketsPageHandle)
                .andRoute(GET("/shirts"), handler::shirtsPageHandle)
                      .andRoute(GET("/"), handler::frontPageHandle);
    }

}


