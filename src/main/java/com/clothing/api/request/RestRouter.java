package com.clothing.api.request;

import org.springframework.context.annotation.*;
import org.springframework.web.reactive.function.server.*;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;


@Configuration
public class RestRouter {

    @Bean("webRestRoute")
    public RouterFunction<ServerResponse> restRoute(RestHandler handler) {
        return route(GET("/api/accessories"), handler::accessoriesRestHandle)
               .andRoute(GET("/api/jackets"), handler::jacketsRestHandle)
                .andRoute(GET("/api/shirts"), handler::shirtsRestHandle);
    }

}


