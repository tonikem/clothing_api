package com.clothing.api.request;

import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.reactive.function.server.*;
import org.springframework.stereotype.Component;
import reactor.core.publisher.*;

import static com.clothing.api.service.FetchService.fetchProductsByCategory;
import static org.springframework.http.MediaType.APPLICATION_STREAM_JSON;


@Component
public class RestHandler {

    private volatile Flux<?> accessoriesFlux = Flux.empty();
    private volatile Flux<?> jacketsFlux = Flux.empty();
    private volatile Flux<?> shirtsFlux = Flux.empty();

    @EventListener
    @Scheduled(fixedRate=300_000)
    public void fetchBadAPI(ContextRefreshedEvent e) {
        this.accessoriesFlux = fetchProductsByCategory("accessories");
        this.jacketsFlux = fetchProductsByCategory("jackets");
        this.shirtsFlux = fetchProductsByCategory("shirts");
    }

    public Mono<ServerResponse> accessoriesRestHandle(ServerRequest req) {
        return ServerResponse.ok().contentType(APPLICATION_STREAM_JSON)
                .body(this.accessoriesFlux, String.class);
    }

    public Mono<ServerResponse> jacketsRestHandle(ServerRequest req) {
        return ServerResponse.ok().contentType(APPLICATION_STREAM_JSON)
                .body(this.jacketsFlux, String.class);
    }

    public Mono<ServerResponse> shirtsRestHandle(ServerRequest req) {
        return ServerResponse.ok().contentType(APPLICATION_STREAM_JSON)
                .body(this.shirtsFlux, String.class);
    }

}


